<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Blocksy
 */

if (
	blocksy_default_akg(
		'page_structure_type',
		blocksy_get_post_options(),
		'default'
	) !== 'default'
	&&
	is_customize_preview()
) {
	blocksy_add_customizer_preview_cache(
		function () {
			return blocksy_html_tag(
				'div',
				[
					'data-structure-custom' => blocksy_default_akg(
						'page_structure_type',
						blocksy_get_post_options(),
						'default'
					)
				],
				''
			);
		}
	);
}


$video_url = get_post_meta(get_the_ID(), 'link_do_video')[0];
$audio_id = get_post_meta(get_the_ID(), 'arquivo_de_audio')[0];
$audio_url = wp_get_attachment_url( $audio_id );
$video = $GLOBALS['wp_embed']->run_shortcode( "[embed width='640' height='360']" . $video_url . "[/embed]" );

?>

<?php

if (have_posts()) {
	the_post();
}

?>

	<?php
		/**
		 * Note to code reviewers: This line doesn't need to be escaped.
		 * Function blocksy_output_hero_section() used here escapes the value properly.
		 */
		echo blocksy_output_hero_section( 'type-2' );
	?>

	<div id="primary" class="content-area">
		<article id="post-<?= get_the_ID() ?>" class="post-958 page type-page status-publish hentry" data-structure="default:wide:normal">			
		<div class="entry-content">
		  <div class="pf-content">
		    <div class="featured_podcast_container">
		      <div class="row">	
			<h2><a href="#"><?php the_title() ?></a></h2>	
			<div class="podcast_video column">		
			  <div class="video_container"><?=$video?></div>
			</div>	
			<div class="podcast_audio column">	
			  <div>
				<?= get_the_content() ?>
			  	<audio controls=""><source src="<?=$audio?>" type="audio/mp3"></audio>
			  </div>	
			</div>	
		     </div>
		   </div>
		</div>



							
		
		
	</article>
	</div>

<?php

blocksy_display_page_elements('separated');

have_posts();
wp_reset_query();

