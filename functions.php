<?php
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_enqueue_styles() {
    $parenthandle = 'blocksy';
    $theme = wp_get_theme();
    wp_enqueue_style( $parenthandle, get_template_directory_uri() . '/style.css',
        array(),
        $theme->parent()->get('Version')
    );
    wp_enqueue_style( 'child-style', get_stylesheet_uri(),
        array( $parenthandle ),
        $theme->get('Version') // this only works if you have Version in the style header
    );
    wp_enqueue_script( 'cdr-script', get_stylesheet_directory_uri() . '/scripts.js', NULL, NULL, True );

}

/* Remove taxonomy name from term page */
add_filter('get_the_archive_title', function ($title) {
    return preg_replace('/^(\w|\s)+: /', '', $title);
});


/* gera a lista de entidades */
add_shortcode( 'entidades', 'cdr_grid_entidades' );
function cdr_grid_entidades( $atts ) {
	$args = array(
		'post_type' => 'entidade',
		'posts_per_page'=> 100,
		'post_status' => 'publish',
		'orderby' => 'name',
		'order' => 'ASC'
	);

	$query = new WP_Query( $args );
	$html = "<div class='entidades_container'>";
	if ($query->have_posts()){
		while( $query->have_posts()) {
			$query->the_post();
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' )[0];
			$link = get_post_meta(get_the_ID(), 'site')[0];
			$html .= "<div>";
			//$html .= "<img src='" . $image . "' />";
			$html .= "<h3><a target='_blank' href='$link'>" . get_the_title() . "</a></h3>";
			$html .= "</div>";
		}
	}
	$html .= "</div>";

	wp_reset_postdata();
	return $html;
}


/* gera a lista de entidades - logos */
add_shortcode( 'entidades_logos', 'cdr_grid_entidades_logos' );
function cdr_grid_entidades_logos( $atts ) {
	$args = array(
		'post_type' => 'entidade',
		'posts_per_page'=> 100,
		'post_status' => 'publish',
		'orderby' => 'name',
		'order' => 'ASC'
	);

	$query = new WP_Query( $args );
	$html = "<div class='entidades_logos_container'>";
	if ($query->have_posts()){
		while( $query->have_posts()) {
			$query->the_post();
			$image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' )[0];
			if (!$image) continue;
			$link = get_post_meta(get_the_ID(), 'site')[0];
			$html .= "<div>";
			$html .= "<h3><a target='_blank' href='$link'><img src='" . $image . "' alt='" . get_the_title() . "' /></a></h3>";
			$html .= "</div>";
		}
	}
	$html .= "</div>";

	wp_reset_postdata();
	return $html;
}

/* gera a lista de podcasts */
add_shortcode( 'podcasts', 'cdr_grid_podcasts' );
function cdr_grid_podcasts( $atts ) {

	$args = array(
		'post_type' => 'podcast',
		'posts_per_page'=> 1,
		'post_status' => 'publish',
	);

	$query = new WP_Query( $args );
	$html = "<div class='featured_podcast_container'>";
	if ($query->have_posts()){
		while( $query->have_posts()) {
			$query->the_post();

			$blocks = parse_blocks( get_the_content() );
			$content = $blocks[0]['innerHTML'];
			$video_url = get_post_meta(get_the_ID(), 'link_do_video')[0];
			$audio_url = get_post_meta(get_the_ID(), 'link_do_audio')[0];
			$video = $GLOBALS['wp_embed']->run_shortcode( "[embed width='640' height='360']" . $video_url . "[/embed]" );


			$html .= "<div class='row'>";
			$html .= '	<h2><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . get_the_title() . '</a></h2>';
			$html .= "	<div class='podcast_video column'>";
			$html .= "		<div class='video_container'>$video</div>";
			$html .= "	</div>";
			$html .= "	<div class='podcast_audio column'>";
			$html .= "	<div>";
			$html .= $content;
			$html .= "<audio controls><source src='$audio_url' type='audio/mp3'></audio>";
			$html .= "	</div>";
			$html .= "	</div>";
			$html .= "</div>";
		}
	}
	$html .= "</div>";

  wp_reset_postdata();

	$args = array(
		'post_type' => 'podcast',
		'posts_per_page'=> 50,
		'post_status' => 'publish',
		'offset' => 1,
	);

	$query = new WP_Query( $args );
	$html .= "<div class='podcasts_container'>";
	if ($query->have_posts()){
		while( $query->have_posts()) {
			$query->the_post();

			$video_url = get_post_meta(get_the_ID(), 'link_do_video')[0];
			$audio_url = get_post_meta(get_the_ID(), 'link_do_audio')[0];
			$video = $GLOBALS['wp_embed']->run_shortcode( "[embed width='320' height='180']" . $video_url . "[/embed]" );

			$html .= "<div class='row'>";
			$html .= "	<div class='podcast_video column'>";
			$html .= $video;
			$html .= "	</div>";
			$html .= "	<div class='podcast_audio column'>";
			$html .= "	<div>";
			$html .= '	<h2><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . get_the_title() . '</a></h2>';
			$html .= "<audio controls><source src='$audio_url' type='audio/mp3'></audio>";
			$html .= "	</div>";
			$html .= "	</div>";
			$html .= "</div>";
		}
	}
	$html .= "</div>";
  wp_reset_postdata();

  return $html;
}

/* inclui postcasts na busca */
/* Atenção: foi preciso incluir a prioridade 11 para entrar depois dos hooks do tema*/
function tg_include_custom_post_types_in_search_results( $query ) {
    if ( $query->is_main_query() && $query->is_search() && ! is_admin() ) {
	    $query->set( 'post_type', array( 'post', 'page', 'podcast' ) );
    }
}
add_action( 'pre_get_posts', 'tg_include_custom_post_types_in_search_results', 11 );

// Página de notícias mostra apenas notícias
function cdr_noticias( $query ) {
    if ( $query->is_main_query() && is_home() ) {
	    $query->set( 'category_name','noticias');
    }
}
add_action( 'pre_get_posts', 'cdr_noticias', 11 );
